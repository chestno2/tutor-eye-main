import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tutor_eye_app/AuthPages/constants.dart';
import 'package:tutor_eye_app/AuthPages/forgotPassword.dart';
import 'package:tutor_eye_app/AuthPages/signUpScreen.dart';
import 'package:tutor_eye_app/pages/widget/bottom_navigation_bar.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
SharedPreferences preffs;
//import 'package:awesomeness.dart';

// ignore: use_key_in_widget_constructors
class EmailLogin extends StatefulWidget {
  //const EmailLogin({Key? key}) : super(key: key);

  @override
  _EmailLoginState createState() => _EmailLoginState();
}

class _EmailLoginState extends State<EmailLogin> {
  //final TextEditingController _textEditingController = TextEditingController();
  bool _clicked = false;
  bool _isobscure = true;
  bool _loginSuccess = false;
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();

  Future get() async {
    preffs = await SharedPreferences.getInstance();
  }
  
  @override
  void initState() {
    get();
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            leading: IconButton(
              icon: Image.asset('assets/images/arrow-left.png'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 15),
              child: SizedBox(
                height: MediaQuery.of(context).size.height-150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [

                    const Text(
                      'Login to TutorEye',
                      style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.w700,
                      ),
                    ),

                    SizedBox(
                      height: MediaQuery.of(context).size.height*0.02,
                    ),

                    Flexible(
                      flex: 7,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [

                          const Text(
                            'Email',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Container(
                            height: 54,
                            decoration: BoxDecoration(
                              color: const Color(0xFFF7F8F8),
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(color: const Color(0x143A4853)),
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: TextField(
                                  controller: _email,
                                  keyboardType: TextInputType.emailAddress,
                                  cursorColor: Colors.black,
                                  textAlignVertical: TextAlignVertical.center,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    prefixIcon: Container(
                                      padding: const EdgeInsets.all(12),
                                      child: Image.asset(
                                        'assets/images/sms2.png',
                                      ),
                                    ),
                                    // Icon(
                                    //   Icons.mail_outline_rounded,
                                    //   color: Color(0x9f292D32),
                                    // ),
                                    //prefixIconColor: Color(0xff292D32),
                                  ),
                                  style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const Text(
                            'Password',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Container(
                            height: 54,
                            decoration: BoxDecoration(
                              color: const Color(0xFFF7F8F8),
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(color: const Color(0x143A4853)),
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: TextField(
                                  controller: _password,
                                  cursorColor: Colors.black,
                                  textAlignVertical: TextAlignVertical.center,
                                  obscureText: _isobscure,
                                  //obscuringCharacter: '*',
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    prefixIcon: Container(
                                      padding: const EdgeInsets.all(12),
                                      child: Image.asset(
                                        'assets/images/lock.png',
                                      ),
                                    ),
                                    // const Icon(
                                    //   Icons.lock_outline_rounded,
                                    //   color: Color(0x9f292D32),
                                    //),
                                    suffixIcon: IconButton(
                                        onPressed: () {
                                          setState(() {
                                            _isobscure = !_isobscure;
                                          });
                                          //_textEditingController.
                                        },
                                        splashColor: Colors.transparent,
                                        icon: Icon(
                                          Icons.remove_red_eye_outlined,
                                          color: _isobscure ? Colors.black : pBlue,
                                        )),
                                  ),
                                  style: const TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: GestureDetector(
                              onTap: (){
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        child: ForgotPassword(),
                                        type: PageTransitionType.rightToLeft));
                              },
                              child: const Text(
                                'Forgot Password?',
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color: pBlue,
                                ),
                              ),
                            ),
                          ),
                          ElevatedButton(
                            onPressed: () async {
                              setState(() {
                                _clicked = true;
                              });
                              _loginSuccess = await emailLogin(_email.text, _password.text);
                              if(_loginSuccess == true) {
                                Navigator.push(
                                    context,
                                    PageTransition(
                                        child: const BottomNav(),
                                        type: PageTransitionType.rightToLeft))
                                    .then((value) => setState(() {
                                  _clicked = false;
                                }));
                              }
                              else
                                {
                                  setState(() {
                                    _clicked = false;
                                  });
                                  /*showDialog(
                                    context: context,
                                    builder: (context) {
                                      return const AlertDialog(
                                        content: Text(
                                            'Username or Password is Incorrect.'),
                                      );
                                    },
                                  );*/
                                }
                            },
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(pBlue),
                            ),
                            child: SizedBox(
                              height: 50,
                              width: double.infinity,
                              child: Center(
                                child: _clicked
                                    ? const SizedBox(
                                  height: 12,
                                  width: 12,
                                  child: CircularProgressIndicator(
                                      strokeWidth: 2.0,
                                      valueColor:
                                      AlwaysStoppedAnimation<Color>(
                                          Colors.white)),
                                )
                                    : const Text(
                                  'Login',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                            ),
                          ),

                          SizedBox(
                            height: MediaQuery.of(context).size.height*0.05,
                          ),

                        ],
                      ),
                    ),
                    Flexible(
                      flex: 4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  PageTransition(
                                      child: const SignUp(),
                                      type: PageTransitionType.rightToLeft));
                            },
                            style: ButtonStyle(
                              shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4.0),
                                    side: const BorderSide(color: pBlue, width: 2.0)),
                              ),
                              backgroundColor:
                              MaterialStateProperty.all<Color>(Colors.white),
                              elevation: MaterialStateProperty.all(0.0),
                            ),
                            child: SizedBox(
                              height: 50,
                              width: double.infinity,
                              child: Center(
                                child: RichText(
                                  text: const TextSpan(
                                      text: "Don't have an account yet? ",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        color: Colors.black,
                                        fontSize: 16,
                                      ),
                                      children: [
                                        TextSpan(
                                          text: 'Sign Up',
                                          style: TextStyle(
                                            color: pBlue,
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                          ),
                                        ),
                                      ]),
                                ),
                              ),
                            ),
                          ),

                          SizedBox(
                            height: MediaQuery.of(context).size.height*0.1,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

Future<bool> emailLogin(String email, String password) async{
  String _uri = "https://stage.tutoreye.com/api/userlogin";

  print(email);
  print(password);

  http.Response Response = await http.post(Uri.parse(_uri), body: {
    'email': email,
    'password': password,
  });
  print(Response.body);
  final int statusCode = Response.statusCode;
  print(statusCode);
  if (statusCode==200) {
    var body = jsonDecode(Response.body);
    try{
      if(body["success"] == true){
        preffs.setString('PK', body["data"]["PK"] );
        preffs.setString('name', body["data"]["name"]);
        preffs.setString('userid', body["data"]["email"]);
        preffs.setString('SK', body["data"]["SK"]);
        preffs.setString('token_id', body["data"]["id_token"]);
        preffs.setString('cognitoId', body["data"]["cognitoId"]);
        preffs.setBool('login', true);
        Fluttertoast.showToast(
          msg: body["message"],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.grey.shade300,
          textColor: Colors.black,
          fontSize: 16.0,
        );
        return true;
      }
    }catch(e){
      if(body["error"]){
        Fluttertoast.showToast(
          msg: body["message"],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.grey.shade300,
          textColor: Colors.black,
          fontSize: 16.0,
        );
        return false;
      }
    }
    Fluttertoast.showToast(
      msg: body["message"],
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.grey.shade300,
      textColor: Colors.black,
      fontSize: 16.0,
    );
    return false;
  } else {
    return false;
  }

}
