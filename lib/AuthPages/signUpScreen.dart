import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tutor_eye_app/AuthPages/constants.dart';
import 'package:tutor_eye_app/AuthPages/loginScreen.dart';
//import 'package:tutor_eye_app/HomePage/HomePage.dart';
import 'package:tutor_eye_app/NavigateFiles/router.dart';
//import 'package:tutor_eye_app/pages/widget/bottom_navigation_bar.dart';
import 'homeScreen.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:tutor_eye_app/AuthPages/otpEmailVerification.dart';
import 'package:fluttertoast/fluttertoast.dart';
String bullet = "\u2022 ";

class SignUp extends StatefulWidget {
  const SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  int _value = 1;

  //final TextEditingController _textEditingController = TextEditingController();
  bool _isobscure = true;
  bool _isnext = false;
  bool a = false;
  bool _signupSuccess = false;
  bool _clicked = false;
  TextEditingController _name = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _parentemail = TextEditingController();

  String dropdownvalue = 'Grade 6';
  String pass = "";
  var items = [
    'Grade 6',
    'Grade 7',
    'Grade 8',
    'Grade 9',
    'Grade 10',
  ];

  bool validate(String p)
  {
    if((p.contains(" ")))
    {
      print('space');
      return false;
    }
    if((p.length < 8))
      {
        print('length');
        return false;
      }
    if (true)
    {
      int count = 0 ;
      for(int i = 0; i <= 9; i++)
      {
        String s = i.toString();
        if(p.contains(s))
          {
            count=1;
            break;
          }
      }

      if(count==0)
        {
          print('digit');
          return false;
        }
    }

    // For special characters
    if (!((p.contains("@") != false) ||
        (p.contains("#") != false) ||
        (p.contains("!") != false) ||
        (p.contains("~") != false) ||
        (p.contains(r"$") != false) ||
        (p.contains("%") != false) ||
        (p.contains("^") != false) ||
        (p.contains("&") != false) ||
        (p.contains("*") != false) ||
        (p.contains("(") != false) ||
        (p.contains(")") != false) ||
        (p.contains("-") != false) ||
        (p.contains("+") != false) ||
        (p.contains("/") != false) ||
        (p.contains(":") != false) ||
        (p.contains(".") != false) ||
        (p.contains(",") != false) ||
        (p.contains("<") != false) ||
        (p.contains(">") != false) ||
        (p.contains("?") != false) ||
        (p.contains("|") != false))) {
      print('special');
      return false;
    }

    if (true)
        {
          int count = 0;
          for(int i = 0; i < p.length; i++)
          {

            if(p.codeUnitAt(i)>=65&&p.codeUnitAt(i)<=90) {
              count = 1;
              break;
            }
          }
          if (count == 0) {
            print('uppercase');
            return false;
        }
        }

    if (true) {
      int count = 0;
      for (int i = 0; i < p.length; i++) {
        if (p.codeUnitAt(i) >= 97 && p.codeUnitAt(i) <= 122) {
          count = 1;
          break;
        }

      }
      if (count == 0) {
        print('lowercase');
        return false;
      }
      }
        // If all conditions fails
        return true;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
        }
      },
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            leading: IconButton(
              icon: Image.asset('assets/images/arrow-left.png'),
              onPressed: () {
                _isnext == true
                    ? setState(() {
                        _isnext = false;
                      })
                    : Navigator.pop(context);
              },
            ),
          ),
          body: SingleChildScrollView(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 25.0, horizontal: 15),
              child: SizedBox(
                height: MediaQuery.of(context).size.height - 150,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Sign up to TutorEye',
                      style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.05,
                    ),
                    Flexible(
                      flex: 7,
                      child: _isnext == true
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "I'm a",
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: MyRadioListTile<int>(
                                        value: 1,
                                        groupValue: _value,
                                        title: 'Student',
                                        onChanged: (value) =>
                                            setState(() => _value = value),
                                      ),
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          0.07,
                                    ),
                                    Expanded(
                                      child: MyRadioListTile<int>(
                                        value: 2,
                                        groupValue: _value,
                                        title: 'Parent',
                                        onChanged: (value) =>
                                            setState(() => _value = value),
                                      ),
                                    ),
                                  ],
                                ),
                                const Text(
                                  'Select Grade',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Container(
                                  height: 54,
                                  decoration: BoxDecoration(
                                    color: const Color(0xFFF7F8F8),
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                        color: const Color(0x143A4853)),
                                  ),
                                  child: Center(
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width -
                                          60,
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(
                                          elevation: 8,
                                          // Initial Value
                                          value: dropdownvalue,
                                          isExpanded: true,
                                          underline: null,
                                          borderRadius:
                                              BorderRadius.circular(8.0),
                                          style: const TextStyle(
                                            fontSize: 16,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400,
                                          ),

                                          // Down Arrow Icon
                                          icon: const Icon(
                                              Icons.keyboard_arrow_down),

                                          // Array list of items
                                          items: items.map((String items) {
                                            return DropdownMenuItem(
                                              value: items,
                                              child: Text(items),
                                            );
                                          }).toList(),
                                          // After selecting the desired option,it will
                                          // change button value to selected value
                                          onChanged: (String newValue) {
                                            setState(() {
                                              dropdownvalue = newValue;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                const Text(
                                  'Your Parent Email',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Container(
                                  height: 54,
                                  decoration: BoxDecoration(
                                    color: const Color(0xFFF7F8F8),
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                        color: const Color(0x143A4853)),
                                  ),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: TextField(
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        controller: _parentemail,
                                        textAlignVertical:
                                            TextAlignVertical.center,
                                        cursorColor: Colors.black,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          prefixIcon: Container(
                                            padding: const EdgeInsets.all(12),
                                            child: Image.asset(
                                              'assets/images/sms2.png',
                                            ),
                                          ),
                                          // Icon(
                                          //   Icons.mail_outline_rounded,
                                          //   color: Color(0x9f292D32),
                                          // ),
                                          //prefixIconColor: Color(0xff292D32),
                                        ),
                                        style: const TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                /*(a==0||a==-1)?Container()
                                    :Container(
                                  child: const Text(
                                    'Password does not matches the valid criteria.',
                                    style: TextStyle(
                                      color: Colors.red,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                )*/
                              ],
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  'Full Name',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Container(
                                  height: 54,
                                  decoration: BoxDecoration(
                                    color: const Color(0xFFF7F8F8),
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                        color: const Color(0x143A4853)),
                                  ),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: TextField(
                                        textCapitalization:
                                            TextCapitalization.words,
                                        controller: _name,
                                        cursorColor: Colors.black,
                                        textAlignVertical:
                                            TextAlignVertical.center,
                                        decoration: InputDecoration(
                                          hintText: 'John Doe',
                                          hintStyle: const TextStyle(
                                            fontSize: 16,
                                          ),
                                          border: InputBorder.none,
                                          prefixIcon: Container(
                                            padding: const EdgeInsets.all(12),
                                            child: Image.asset(
                                              'assets/images/account.png',
                                            ),
                                          ),
                                          // Icon(
                                          //   Icons.mail_outline_rounded,
                                          //   color: Color(0x9f292D32),
                                          // ),
                                          //prefixIconColor: Color(0xff292D32),
                                        ),
                                        style: const TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                const Text(
                                  'Email',
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Container(
                                  height: 54,
                                  decoration: BoxDecoration(
                                    color: const Color(0xFFF7F8F8),
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                        color: const Color(0x143A4853)),
                                  ),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: TextField(
                                        keyboardType:
                                            TextInputType.emailAddress,
                                        controller: _email,
                                        cursorColor: Colors.black,
                                        textAlignVertical:
                                            TextAlignVertical.center,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          prefixIcon: Container(
                                            padding: const EdgeInsets.all(12),
                                            child: Image.asset(
                                              'assets/images/sms2.png',
                                            ),
                                          ),
                                          // Icon(
                                          //   Icons.mail_outline_rounded,
                                          //   color: Color(0x9f292D32),
                                          // ),
                                          //prefixIconColor: Color(0xff292D32),
                                        ),
                                        style: const TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      'Password',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                const SizedBox(
                                  width: 10,
                                ),
                                Container(
                                  //alignment: Alignment.topRight,
                                  //alignment: Alignment.centerRight,
                                  margin: const EdgeInsets.only(bottom:5),
                                  child: GestureDetector(
                                    onTap: () {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: true,
                                          builder: (context) {
                                            return InstructionPopup();
                                          });
                                    },
                                    child: const Padding(
                                      padding: EdgeInsets.only(top: 10.0,right:24.0, bottom: 5),
                                      child: Icon(
                                        Icons.info_outline,
                                        color: Colors.black,
                                        size: 16,
                                      ),
                                    ),
                                  ),
                                ),
                                ],
                                ),
                                Container(
                                  height: 54,
                                  decoration: BoxDecoration(
                                    color: const Color(0xFFF7F8F8),
                                    borderRadius: BorderRadius.circular(4.0),
                                    border: Border.all(
                                        color: const Color(0x143A4853)),
                                  ),
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(2.0),
                                      child: TextField(
                                        controller: _password,
                                        cursorColor: Colors.black,
                                        /*maxLength: 8,
                                        inputFormatters: [
                                          LengthLimitingTextInputFormatter(8),
                                        ],*/
                                        textAlignVertical:
                                            TextAlignVertical.center,
                                        obscureText: _isobscure,
                                        //obscuringCharacter: '*',
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          prefixIcon: Container(
                                            padding: const EdgeInsets.all(12),
                                            child: Image.asset(
                                              'assets/images/lock.png',
                                            ),
                                          ),
                                          // const Icon(
                                          //   Icons.lock_outline_rounded,
                                          //   color: Color(0x9f292D32),
                                          //),
                                          suffixIcon: IconButton(
                                              onPressed: () {
                                                setState(() {
                                                  _isobscure = !_isobscure;
                                                });
                                                //_textEditingController.
                                              },
                                              splashColor: Colors.transparent,
                                              icon: Icon(
                                                Icons.remove_red_eye_outlined,
                                                color: _isobscure
                                                    ? Colors.black
                                                    : pBlue,
                                              )),
                                        ),
                                        style: const TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                /*Container(
                                  child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    //Message(a),
                                    '',
                                    style: const TextStyle(
                                      color: Colors.red,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ),),*/
                                /*a==-1?Container()
                                    :a==0?Container()
                                    :a==1?Container(
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'The password should contain at least 8 and at most 15 letters.',
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ),
                                )
                                    :a==2?Container(
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'The password should not contain spaces.',
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ),
                                )
                                    :a==3?Container(
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'The password should contain at least one digit between 0-9.',
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ),
                                )
                                    :a==4?Container(
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'The password should contain at least one special character.',
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ),
                                )
                                    :a==5?Container(
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'The password should contain at least one upper case character A-Z.',
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ),
                                )
                                    :Container(
                                  child: const Padding(
                                    padding: EdgeInsets.all(8.0),
                                    child: Text(
                                      'The password should contain at least one lower case character a-z.',
                                      style: TextStyle(
                                        color: Colors.red,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ),
                                )*/
                              ],
                            ),
                    ),
                    Flexible(
                      flex: 4,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          ElevatedButton(
                            onPressed: () async {
                              _isnext == true
                                  ? {
                                //pass = trim(_password.toString()),
                                //pass = _password.toString().replaceAll(" ", "").toString(),
                              setState(() {
                              _clicked = true;
                              }),
                                print(_password.text),
                                //print(pass),
                                a = validate(_password.text),
                                print('a:$a'),
                                    if(a)
                                      {
                                        _signupSuccess = await register(_email.text, _password.text, _name.text),
                                        print(_signupSuccess),
                                        if( _signupSuccess == true )
                                          {
                                            Navigator.push(
                                                context,
                                                PageTransition(
                                                    child: OTPVerification(_email.text,dropdownvalue, _value,_password.text,_name.text),
                                                    type: PageTransitionType.rightToLeft)).then((value) => setState(() {
                                          _clicked = false;
                                          }))

                                          }
                                        else
                                          {
                                          setState(() {
                                          _clicked = false;})
                                          }
                                    }
                                    else
                                      {
                                        Fluttertoast.showToast(
                                          msg: "Password does not match the valid criteria.",
                                          //msg: "toast",
                                          toastLength: Toast.LENGTH_SHORT,
                                          gravity: ToastGravity.BOTTOM,
                                          timeInSecForIosWeb: 1,
                                          backgroundColor: Colors.grey.shade300,
                                          textColor: Colors.black,
                                          fontSize: 16.0,
                                        ),
                                        setState(() {
                                          _clicked = false;
                                        }),
                                        /*showDialog(
                                          context: context,
                                          builder: (context) {
                                            return const AlertDialog(
                                              content: Text(
                                                  'Password does not match the valid criteria.'),
                                            );
                                          },
                                        ),*/
                                      },
                              //_signupSuccess = register(_email.toString(), _password.toString(), _name.toString()),
                                    /*else
                                      {
                                        Message(a)
                                      }*/
                                    
                                    //register(_email.toString(), _password.toString(), _name.toString()),
                                    //Navigate.to(const BottomNav())
                                /*Navigator.push(
                                    context,
                                    PageTransition(
                                        child: OTPVerification(_email.toString()),
                                        type: PageTransitionType.rightToLeft))*/

                              }
                                  :
                                    setState(() {
                                      FocusScopeNode currentFocus =
                                      FocusScope.of(context);

                                      if (!currentFocus.hasPrimaryFocus) {
                                        currentFocus.unfocus();
                                      }

                                      _isnext = !_isnext;
                                    });
                            },
                            style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all<Color>(pBlue),
                            ),
                            child: SizedBox(
                              height: 50,
                              width: double.infinity,
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 8.0),
                                child: _isnext == true
                                    ? Center(
                                        child: _clicked
                                            ? const SizedBox(
                                          height: 12,
                                          width: 12,
                                          child: CircularProgressIndicator(
                                              strokeWidth: 2.0,
                                              valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Colors.white)),
                                        )
                                            : const Text(
                                          'Register',
                                          style: TextStyle(
                                            fontWeight: FontWeight.w700,
                                            fontSize: 16,
                                          ),
                                        ),
                                      )
                                    : Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: const [
                                          Text(
                                            'Next',
                                            style: TextStyle(
                                              fontWeight: FontWeight.w700,
                                              fontSize: 16,
                                            ),
                                          ),
                                          Icon(
                                            Icons.arrow_forward_sharp,
                                            color: Colors.white,
                                          )
                                        ],
                                      ),
                              ),
                            ),
                          ),
                          Center(
                            child: RichText(
                              text: TextSpan(
                                  text: "Already have an account? ",
                                  style: const TextStyle(
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black,
                                    fontSize: 16,
                                  ),
                                  children: [
                                    TextSpan(
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {
                                          Navigator.pushAndRemoveUntil(
                                            context,
                                            PageTransition(
                                              child: const LoginPage(),
                                              type: PageTransitionType
                                                  .rightToLeft,
                                            ),
                                            ModalRoute.withName('/'),
                                          );
                                        },
                                      text: 'LOGIN',
                                      style: const TextStyle(
                                        color: pBlue,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ]),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class MyRadioListTile<T> extends StatelessWidget {
  final T value;
  final T groupValue;
  final String title;
  final ValueChanged<T> onChanged;

  // ignore: prefer_const_constructors_in_immutables, use_key_in_widget_constructors
  MyRadioListTile({
    this.value,
    this.groupValue,
    this.onChanged,
    this.title,
  });

  @override
  Widget build(BuildContext context) {
    final title = this.title;
    final isSelected = value == groupValue;
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () => onChanged(value),
      child: Container(
        height: 54,
        decoration: BoxDecoration(
          color: const Color(0xFFF7F8F8),
          borderRadius: BorderRadius.circular(50.0),
          border: isSelected
              ? Border.all(color: pBlue, width: 2.0)
              : Border.all(color: const Color(0x143A4853)),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _customRadioButton,
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w700,
                  color: isSelected ? pBlue : Colors.black,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget get _customRadioButton {
    final isSelected = value == groupValue;
    return Container(
      padding:
          isSelected ? const EdgeInsets.all(6.0) : const EdgeInsets.all(11.0),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        //color: isSelected ? Colors.blue : null,
        //borderRadius: BorderRadius.circular(4),
        border: Border.all(
          color: isSelected ? Colors.blue : Colors.black,
          width: isSelected ? 8.0 : 2.0,
        ),
      ),
    );
  }
}

Future<bool> register(String email, String password , String name) async{
  String _uri = "https://stage.tutoreye.com/api/userlogin";

  print(email);
  print(password);
  print(name);
  http.Response Response = await http.post(Uri.parse(_uri), body: {
    'email': email,
    'password': password,
    'name': name,
  });
  print(Response.body);
  final int statusCode = Response.statusCode;
  print(statusCode);
  if (statusCode==200) {
    var body = jsonDecode(Response.body);
    try{
      if(body["success"] == true){
        Fluttertoast.showToast(
          msg: body["message"],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.grey.shade300,
          textColor: Colors.black,
          fontSize: 16.0,
        );
        return true;
      }
    }catch(e){
      if(body["error"]){
        Fluttertoast.showToast(
          msg: body["message"].toString(),
          //msg: "toast",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 5,
          backgroundColor: Colors.grey.shade300,
          textColor: Colors.black,
          fontSize: 16.0,
        );
        //print(body["message"]);
        return false;
      }
    }
    Fluttertoast.showToast(
      msg: body["message"],
      //msg: "toast",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.grey.shade300,
      textColor: Colors.black,
      fontSize: 16.0,
    );
    //print(body["message"]);
    return false;
  } else {
    return false;
  }

}

class InstructionPopup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      content: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
              left: 0.0, top: 0.0, right: 0.0, bottom: 0.0),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.7,
            child: Column(
              children: const[
                Center(
                    child: Text(
                      'Password Validations',
                      style: TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.bold
                      ),
                    )
                ),
                Divider(
                  color: Colors.black,
                  height: 20,
                  thickness: 2,
                  indent: 10,
                  endIndent: 10,
                ),
                Padding(
                    padding: EdgeInsets.all(8),
                  child: Text(
                      "\u2022 Password should not contain spaces\n" +
                      "\u2022 Password should have a minimum length 8\n" +
                      "\u2022 Password should contain at least one digit(0-9)\n" +
                      "\u2022 Password should contain at least one Upper Case Letter(A-Z)\n"+
                      "\u2022 Password should contain at least one Lower Case Letter(a-z)\n"+
                      "\u2022 Password should contain at least one special character '@#!~%^&*()-+/:.,<>?|"+r"$"+"'",
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
