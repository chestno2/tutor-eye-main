import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:page_transition/page_transition.dart';
import 'package:tutor_eye_app/AuthPages/splashScreen.dart';
import 'package:tutor_eye_app/NavigateFiles/router.dart';
import 'package:tutor_eye_app/pages/widget/bottom_navigation_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';
SharedPreferences loginPreffs;
bool login=false;
class FirstPage_SplashScreen extends StatefulWidget {
  const FirstPage_SplashScreen({Key key}) : super(key: key);

  @override
  _FirstPage_SplashScreenState createState() => _FirstPage_SplashScreenState();
}

class _FirstPage_SplashScreenState extends State<FirstPage_SplashScreen> {

  Future get() async {
    loginPreffs = await SharedPreferences.getInstance();
    var temp = loginPreffs.getBool('login');
    if (temp == null || temp == false) {
      login = false;
      loginPreffs.setBool('login', false);
    } else
    {
      login = true;
    }
  }

  @override
  void initState() {
    super.initState();
    get();
    Timer(Duration(seconds: 2), () =>
        Navigator.push(
          context,
          PageTransition(
            duration: Duration(milliseconds: 200),
            type: PageTransitionType.rightToLeft,
            child: login==false?const SplashScreen():const BottomNav(),
                )
            ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Image.asset(
        "assets/images/Splash.jpg",
        height: 300.sh,
        width: MediaQuery.of(context).size.width,
      ),
    );
  }
}

